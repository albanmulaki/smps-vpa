@extends('admin.index')
@section('content')
<div id="indexcont">
            <div id="firstco">
                <ul>
                    <li><h2>჻ Perdorim i leht</h2>
                        <div>Ne kete verzion perdorimi eshte shum me i thjesht dhe i leht mundesia per te ber pyetje eshte me e thjesht se me par.</div></li>
                    <li><h2>๛ Pyetje &amp; Pergjigje</h2>
                        <div>Mund te beni pyetje te ndryshme ku do te merrni mbeshtetje brenda nje kohe te shkurter.</div></li>
                    <li><h2>౾ Tutoriale</h2>
                        <div>Mund te gjeni tutoriale te ndryshme tutorialet te cilat verifikohen dhe kompletohen pothuajse te ngjashme si libra ato tutoriale behen zyrtare ne OBJprog dhe vendosen tek vendi i tutorialve te OBJprog.</div></li>
                    <li><h2>☆ Reputacioni</h2>
                        <div>Reputacioni ndikon mjaft mir si ne komunitet gjithashtu edhe tek ju nese keni nje reputacion mjaft te madh ne OBJprog do keni mundesi te kqyeni menjeher ne grupin e OBJprog. Reputacionet mund te mirren nga perdoruesit e tjer ku qdokush qe lexon temen tuaj e vlerson. </div></li>
           
                </ul>
            </div>       </div>

@stop